#!/usr/bin/env node

const cli = require('../lib/cli.js')

const clargs = cli.getClargs()

if (clargs.output) {
  console.log('Output = ' + clargs.output)
}

if (clargs._) {
  console.log('Input = ' + clargs._)
}
