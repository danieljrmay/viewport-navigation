function main () {
  try {
    console.log('viewport-navigation-cli\n')
    const clargs = getClargs()
    const debugMode = clargs.debug

    debugMsg('Running in debugging mode', debugMode)
    debugMsg('outputdir=' + clargs.outputdir, debugMode)
    debugMsg('input files=' + clargs._.join(', '), debugMode)

    createOutputDirectory(clargs.outputdir, debugMode)

    for (const inputPath of clargs._) {
      const inputDocument = getDocument(inputPath, debugMode)

      switch (inputDocument.documentElement.nodeName) {
        case 'html':
          debugMsg(inputPath + ' is an XHTML document', debugMode)
          break
        case 'contents':
          debugMsg(inputPath + ' is a contents XML document', debugMode)
          break
        default:
          throw Error(inputPath + ' is an unknown XML document')
      }
    }

    process.exit(0)
  } catch (err) {
    console.log(err)
    process.exit(1)
  }
}

if (require.main === module) {
  main()
}

function debugMsg (msg, debugMode) {
  if (debugMode) {
    console.log(msg)
  }
}

function getClargs (customArgv = null) {
  const clargs = require('yargs')
    .usage(
      'Usage:\n  $0 [OPTIONS] FILE.xhtml' +
        '\n  $0 [OPTIONS] contents.xml'
    )
    .option('debug', {
      alias: 'd',
      type: 'boolean',
      default: false,
      description: 'Output debugging messages'
    })
    .option('outputdir', {
      alias: 'o',
      type: 'string',
      description: 'Save output files to the specified directory',
      default: 'vn-out'
    })
    .check(function (argv, options) {
      if (argv._.length >= 1) {
        return true
      } else {
        throw Error('You must specify at least one input file.')
      }
    })
    .help('help')
    .alias('help', 'h')
    .epilogue('For more information see https://danieljrmay.gitlab.io/viewport-navigation/index.xhtml')
    .strict()

  if (customArgv === null) {
    return clargs.parse()
  } else {
    return clargs.parse(customArgv)
  }
}

function createOutputDirectory (path, debugMode) {
  const fs = require('fs')

  try {
    fs.mkdirSync(path)
    debugMsg('Created directory ' + path, debugMode)
  } catch (err) {
    if (err.code === 'EEXIST') {
      if (!fs.statSync(path).isDirectory()) {
        throw Error('The path ' + path + ' already exists and is not a directory.')
      }
      debugMsg('A directory already exists at ' + path, debugMode)
    } else {
      debugMsg('A directory already exists at ' + path, debugMode)
      throw err
    }
  }
}

function getDocument (path, debugMode) {
  const fs = require('fs')
  const xmldom = require('xmldom')
  const domParser = new xmldom.DOMParser()

  return domParser.parseFromString(fs.readFileSync(path, 'utf-8'))
}

module.exports = getClargs
