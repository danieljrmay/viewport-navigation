/*
 * viewport-navigation.js - A JavaScript enhanced navigation menu
 * which allows you to see where you are in a long HTML/XHTML
 * document.
 *
 * Copyright 2019 Daniel J. R. May.
 *
 * This file is part of viewport-navigation.
 *
 */

const VERSION = '0.20.2'

const NARROW_DISPLAY_MAX_WIDTH = 1080

const KEY_CODE_PAGE_UP = 33
const KEY_CODE_PAGE_DOWN = 34
const KEY_CODE_END = 35
const KEY_CODE_HOME = 36
const KEY_CODE_ARROW_LEFT = 37
const KEY_CODE_ARROW_UP = 38
const KEY_CODE_ARROW_RIGHT = 39
const KEY_CODE_ARROW_DOWN = 40
const KEY_CODE_B = 66
const KEY_CODE_D = 68
const KEY_CODE_F = 70
const KEY_CODE_L = 76
const KEY_CODE_N = 78
const KEY_CODE_P = 80
const KEY_CODE_S = 83

/**
 * A class which provides the following services to a document:
 * <ul>
 * <li>An auto-generated navigation menu with a viewbox which shows
 * the user what sections of a document are currently within the
 * viewport of their browser.</li>
 * <li>A slideshow mode.</li>
 * <li>A fullscreen mode.</li>
 * <li>User selectable dark and light themes.</li>
 * <li>A blank screen mode.</li>
 * </ul>
 * @implements {EventListener}
 */
class ViewportNavigation {
  /**
   * Constructor.
   * @param {Document} currentDocument - The document to create a
   * viewport navigation menu for, defaults to the current browser
   * document.
   * @param {string} sectioningElementSelector - The selector to use
   * to detect sectioning elements, defaults to 'section'.
   * @param {string} listTagName - The list tag to use when generating
   * the navigation menu, defaults to 'ul'.
   */
  constructor (
    currentDocument = document,
    sectioningElementSelector = 'section, article, aside',
    listTagName = 'ul'
  ) {
    this._currentDocument = currentDocument
    this._sectioningElementSelector = sectioningElementSelector
    this._listTagName = listTagName
    this._lastNonBlankDisplayMode = 'normal'

    /* The DOMContentLoaded event fires when the initial document
     * has been completely loaded and parsed, without waiting for
     * stylesheets, images, and subframes to finish loading.
     */
    this._currentDocument.addEventListener('DOMContentLoaded', this)

    /* The load event is fired when the whole page has loaded,
     * including all dependent resources such as stylesheets
     * images.
     */
    this._currentDocument.defaultView.addEventListener('load', this)

    /* Members which will be set later. */
    this._slides = []
    this._currentSlideIndex = 0
    this._lastSlideIndex = -1

    this._navInnerHTML = ''

    this.menuBar = null

    this._navMenu = null
    this._navMenuButton = null
    this._navMenuContent = null
    this._controlsMenu = null
    this._controlsMenuButton = null

    this._normalDisplayInput = null
    this._fullscreenDisplayInput = null
    this._slideshowDisplayInput = null
    this._presentationDisplayInput = null
    this._blankDisplayInput = null

    this._defaultThemeInput = null
    this._lightThemeInput = null
    this._darkThemeInput = null

    this._viewbox = null

    this._rootElementClasses = null

    this._swipeDetector = new SwipeDetector(100)
  }

  /**
   * Implementation of the EventListener interface. This method is
   * called by the user agent when an event is sent to instances of
   * the ViewportNavigation class.
   * @param {Event} event - The event object to be processed.
   */
  handleEvent (event) {
    /* We check the event by type. Commonest to rarest. */
    switch (event.type) {
      case 'scroll':
        this.updateViewbox()
        break
      case 'resize':
        this.updateViewbox()
        break
      case 'keydown':
        this._handleKeydownEvent(event)
        break
      case 'click':
        switch (event.target) {
          case this._navMenuButton:
            this.toggleNavMenu()
            break
          case this._controlsMenuButton:
            this.toggleControlsMenu()
            break
          default:
            if (event.target instanceof HTMLAnchorElement && this._navMenuContent.contains(event.target)) {
              if (this.isSlideshowMode()) {
                try {
                  const url = new URL(event.target.href)

                  if (url.hash !== '') {
                    const targetElementId = url.hash.substring(1)
                    const targetElement = this._currentDocument.getElementById(targetElementId)
                    const containingSlideIndex = this.getContainingSlideIndex(targetElement)
                    this.showSlide(containingSlideIndex)
                    this.closeNavMenu()
                    targetElement.scrollIntoView()
                  }
                } catch (error) {
                  console.log('Not changing current slide because: ' + error)
                }
              } else if (this.isNavMenuTypicallyClosed()) {
                this.closeNavMenu()
              }
            } else if (this.isBlankMode()) {
              this.setDisplay(this._lastNonBlankDisplayMode)
            }
        }
        break
      case 'change':
        switch (event.target) {
          case this._normalDisplayInput:
          case this._fullscreenDisplayInput:
          case this._slideshowDisplayInput:
          case this._presentationDisplayInput:
          case this._blankDisplayInput:
            this.closeControlsMenu()
            this.setDisplay(event.target.value)
            break
          case this._defaultThemeInput:
          case this._darkThemeInput:
          case this._lightThemeInput:
            this.closeControlsMenu()
            this.setTheme(event.target.value)
            break
          default:
            break
        }
        break
      case 'touchstart':
        if (this.isSlideshowMode()) {
          event.preventDefault()
          this._swipeDetector.addTouchEvent(event)
        }
        break
      case 'touchend':
        if (this.isSlideshowMode()) {
          this._swipeDetector.addTouchEvent(event)
          if (this._swipeDetector.isSwipeLeft()) {
            this.showNextSlide()
          } else if (this._swipeDetector.isSwipeRight()) {
            this.showPreviousSlide()
          }
        }
        break
      case 'touchcancel':
        this._swipeDetector.addTouchEvent(event)
        break
      case 'DOMContentLoaded':
        this._rootElementClasses = this._currentDocument.documentElement.classList
        this._createHeader()
        this._initSlides()
        break
      case 'load':
        this._normalDisplayInput.addEventListener('change', this)
        this._fullscreenDisplayInput.addEventListener('change', this)
        this._slideshowDisplayInput.addEventListener('change', this)
        this._presentationDisplayInput.addEventListener('change', this)
        this._blankDisplayInput.addEventListener('change', this)
        this._defaultThemeInput.addEventListener('change', this)
        this._darkThemeInput.addEventListener('change', this)
        this._lightThemeInput.addEventListener('change', this)
        this._currentDocument.addEventListener('click', this)
        this._currentDocument.addEventListener('keydown', this)
        this._currentDocument.addEventListener('touchstart', this)
        this._currentDocument.addEventListener('touchend', this)
        this._currentDocument.addEventListener('touchcancel', this)
        this._currentDocument.defaultView.addEventListener('scroll', this)
        this._currentDocument.defaultView.addEventListener('resize', this)
        this.updateViewbox()
        break
      default:
        throw Error('Unhandled event of type: ' + event.type)
    }
  }

  /**
   * Get the slideIndex of the slide which contains the specified element.
   *
   * @param {Element} element - The element to attempt to find the containing slideIndex for.
   * @return {number} The slide index of the slide which contains the element.
   * @throws {Error} If there is no corresponding slide.
   */
  getContainingSlideIndex (element) {
    if (element === this._currentDocument.documentElement) {
      throw Error('Element is document root element')
    }

    if (element.classList.contains('_vn-slide')) {
      for (let i = 0; i <= this._lastSlideIndex; i++) {
        if (this._slides[i] === element) {
          return i
        }
      }

      throw Error('Element is not in _slides array event though is has the _vn-slide class.')
    } else {
      return this.getContainingSlideIndex(element.parentElement)
    }
  }

  /**
   * @return {boolean} True if the display mode of the current
   * document is 'blank', false otherwise.
   */
  isBlankMode () {
    return this._rootElementClasses.contains('_vn-blank')
  }

  /**
   * @return {boolean} True if the display mode of the current
   * document is 'slideshow', false otherwise.
   */
  isSlideshowMode () {
    return this._rootElementClasses.contains('_vn-slideshow')
  }

  /**
   * Open the navigation menu.
   */
  openNavMenu () {
    this.hideControlsMenu()
    this._navMenu.classList.add('_vn-menu-open')
    this.updateViewbox()
  }

  /**
   * Close the navigation menu.
   */
  closeNavMenu () {
    this.showControlsMenu()
    this._navMenu.classList.remove('_vn-menu-open')
  }

  /**
   * Toggle the navigation menu.
   */
  toggleNavMenu () {
    if (this._navMenu.classList.contains('_vn-menu-open')) {
      this.closeNavMenu()
    } else {
      this.openNavMenu()
    }
  }

  /**
   * Open the controls menu.
   */
  openControlsMenu () {
    this._controlsMenu.classList.add('_vn-menu-open')
  }

  /**
   * Get whether the navigation menu is typically closed, this should
   * be the case for smaller screens.
   */
  isNavMenuTypicallyClosed () {
    if (window.innerWidth <= NARROW_DISPLAY_MAX_WIDTH) {
      return true
    } else {
      return false
    }
  }

  /**
   * Close the controls menu.
   */
  closeControlsMenu () {
    this._controlsMenu.classList.remove('_vn-menu-open')
  }

  /**
   * Toggle the controls menu.
   */
  toggleControlsMenu () {
    if (this._controlsMenu.classList.contains('_vn-menu-open')) {
      this.closeControlsMenu()
    } else {
      this.openControlsMenu()
    }
  }

  /**
   * Hide the controls menu.
   */
  hideControlsMenu () {
    this._controlsMenu.classList.add('_vn-hide')
  }

  /**
   * Show the controls menu.
   */
  showControlsMenu () {
    this._controlsMenu.classList.remove('_vn-hide')
  }

  /**
   * Set the current display mode.
   *
   * @param {string} display - One of 'blank', 'fullscreen',
   * 'presentation', 'slideshow' or 'normal'. Any other value will
   * default to 'normal'.
   */
  setDisplay (display) {
    switch (display) {
      case 'blank':
        this._rootElementClasses.add('_vn-blank')
        this._blankDisplayInput.checked = true
        return
      case 'fullscreen':
        this._enableFullscreen()
        this._rootElementClasses.remove('_vn-blank', '_vn-slideshow')
        this._fullscreenDisplayInput.checked = true
        break
      case 'presentation':
        this._enableFullscreen()
        if (this.isBlankMode()) {
          this._rootElementClasses.remove('_vn-blank')
        } else {
          this.setFirstSlideInViewToCurrentSlide()
        }
        this._rootElementClasses.add('_vn-slideshow')
        this._presentationDisplayInput.checked = true
        break
      case 'slideshow':
        if (this.isBlankMode()) {
          this._rootElementClasses.remove('_vn-blank')
        } else {
          this.setFirstSlideInViewToCurrentSlide()
        }
        this._disableFullscreen()
        this._rootElementClasses.add('_vn-slideshow')
        this._slideshowDisplayInput.checked = true
        break
      case 'normal':
      default:
        this._rootElementClasses.remove('_vn-blank', '_vn-slideshow')
        this._disableFullscreen()
        this._normalDisplayInput.checked = true
        this.scrollToCurrentSlide()
        this.updateViewbox()
    }

    this._lastNonBlankDisplayMode = display
  }

  /**
   * Set the current theme.
   *
   * @param {string} theme - One of 'dark', 'light' or 'default'. Any
   * other value will result in the 'default' theme being set.
   */
  setTheme (theme) {
    switch (theme) {
      case 'dark':
        this._rootElementClasses.remove('_vn-light')
        this._rootElementClasses.add('_vn-dark')
        this._darkThemeInput.checked = true
        break
      case 'light':
        this._rootElementClasses.remove('_vn-dark')
        this._rootElementClasses.add('_vn-light')
        this._lightThemeInput.checked = true
        break
      case 'default':
      default:
        this._rootElementClasses.remove('_vn-dark')
        this._rootElementClasses.remove('_vn-light')
        this._defaultThemeInput.checked = true
        break
    }
  }

  /**
   * Enable fullscreen mode, if possible.
   */
  _enableFullscreen () {
    if (this._currentDocument.fullscreenEnabled) {
      if (this._currentDocument.fullscreenElement === null) {
        this._currentDocument.documentElement.requestFullscreen()
      }
    } else {
      console.log('Fullscreen mode is not available.')
    }
  }

  /**
   * Disable fullscreen mode, this disables presentation mode as well.
   */
  _disableFullscreen () {
    if (this._currentDocument.fullscreenElement !== null) {
      this._currentDocument.exitFullscreen()
    }
  }

  /**
   * Set the current slide. If the provided <code>slideIndex</code>
   * does not exist then this method performs no action.
   * @param {number} slideIndex - The index of the new current slide.
   */
  showSlide (slideIndex) {
    if (slideIndex >= 0 && slideIndex <= this._lastSlideIndex) {
      for (let i = 0; i <= this._lastSlideIndex; i++) {
        if (i === slideIndex) {
          this._slides[i].classList.remove('_vn-inactive')
          this._slides[i].classList.add('_vn-active')
        } else if (this._slides[i].contains(this._slides[slideIndex])) {
          this._slides[i].classList.remove('_vn-inactive')
          this._slides[i].classList.add('_vn-active')
        } else {
          this._slides[i].classList.remove('_vn-active')
          this._slides[i].classList.add('_vn-inactive')
        }
      }

      this._currentSlideIndex = slideIndex
      this.updateViewbox()
    }
  }

  /**
   * Set the first slide as the current slide.
   */
  showFirstSlide () {
    this.showSlide(0)
  }

  /**
   * Set the last slide as the current slide.
   */
  showLastSlide () {
    this.showSlide(this._lastSlideIndex)
  }

  /**
   * Show the next slide as the current slide.
   */
  showNextSlide () {
    this.showSlide(this._currentSlideIndex + 1)
  }

  /**
   * Show the previous slide as the current slide.
   */
  showPreviousSlide () {
    this.showSlide(this._currentSlideIndex - 1)
  }

  /**
   * Scroll the viewport so that the current slide is in view.
   */
  scrollToCurrentSlide () {
    this._slides[this._currentSlideIndex].scrollIntoView()
  }

  /**
   * Set the first slide in view to the current slide.
   */
  setFirstSlideInViewToCurrentSlide () {
    for (let i = 0; i <= this._lastSlideIndex; i++) {
      const f = this._getFractionsOfElementOffscreen(this._slides[i])

      if (f[0] < 1) {
        this.showSlide(i)
        break
      }
    }
  }

  /**
   * Update the dimensions of the viewbox.
   */
  updateViewbox () {
    const aList = this._navMenuContent.querySelectorAll('a')
    const aListLength = aList.length
    let viewboxTop = aList[0].offsetTop
    let viewboxBottom = aList[0].offsetTop
    let setViewboxTopInSlideshowMode = false

    for (let i = 0; i < aListLength; i++) {
      const aHref = aList[i].getAttribute('href')

      if (aHref.substring(0, 1) === '#') {
        const refId = aHref.substring(1)
        const section = document.getElementById(refId)
        const f = this._getFractionsOfElementOffscreen(section)

        if (this.isSlideshowMode() && this._lastSlideIndex !== -1) {
          if (this._slides[this._currentSlideIndex].contains(section)) {
            if (!setViewboxTopInSlideshowMode) {
              viewboxTop = aList[i].offsetTop
              setViewboxTopInSlideshowMode = true
            }
          } else {
            continue
          }
        }

        if (f[0] > 0) {
          viewboxTop = aList[i].offsetTop + f[0] * aList[i].offsetHeight
        }

        if (f[1] < 1) {
          viewboxBottom = aList[i].offsetTop + (1 - f[1]) * aList[i].offsetHeight
        }
      }
    }

    /* Viewport height is arithmetic subtraction (viewportBottom - viewportTop). */
    this._viewbox.style = 'top: ' + viewboxTop + 'px; height: ' + (viewboxBottom - viewboxTop) + 'px;'
  }

  /**
   * Log there viewport navigation version number to the console.
   */
  logVersion () {
    console.log('viewport-navigation version ' + VERSION)
  }

  /**
   * Initialize the slideshow parameters.
   */
  _initSlides () {
    this._slides = this._currentDocument.getElementsByClassName('_vn-slide')
    this._lastSlideIndex = this._slides.length - 1

    if (this._lastSlideIndex === -1) {
      /* No slides have been defined. */
      return
    }

    const illegalSlides = this._currentDocument.querySelectorAll('._vn-slide._vn-active, ._vn-slide._vn-inactive')
    if (illegalSlides.length !== 0) {
      throw Error("An element in the initial document with class '_vn-slide' " +
                  "must not also have the classes '_vn-active' or '_vn-inactive'.")
    }

    this._slides[0].classList.add('_vn-active')
    for (let i = 1; i <= this._lastSlideIndex; i++) {
      this._slides[i].classList.add('_vn-inactive')
    }
  }

  /**
   * Handle all keydown events.
   * @param {Event} event - The <code>keydown</code> event to process.
   */
  _handleKeydownEvent (event) {
    if (this.isBlankMode()) {
      this.setDisplay(this._lastNonBlankDisplayMode)
      return
    }

    if (event.ctrlKey && event.shiftKey && event.altKey) {
      switch (event.keyCode) {
        case KEY_CODE_B:
          this.setDisplay('blank')
          return
        case KEY_CODE_D:
          this.setTheme('dark')
          return
        case KEY_CODE_L:
          this.setTheme('light')
          return
        case KEY_CODE_F:
          this.setDisplay('fullscreen')
          return
        case KEY_CODE_N:
          this.setDisplay('normal')
          return
        case KEY_CODE_P:
          this.setDisplay('presentation')
          return
        case KEY_CODE_S:
          this.setDisplay('slideshow')
          return
        default:
          return
      }
    }

    if (this.isSlideshowMode()) {
      switch (event.keyCode) {
        case KEY_CODE_PAGE_UP:
        case KEY_CODE_ARROW_UP:
        case KEY_CODE_ARROW_LEFT:
          this.showPreviousSlide()
          break
        case KEY_CODE_PAGE_DOWN:
        case KEY_CODE_ARROW_DOWN:
        case KEY_CODE_ARROW_RIGHT:
          this.showNextSlide()
          break
        case KEY_CODE_HOME:
          this.showFirstSlide()
          break
        case KEY_CODE_END:
          this.showLastSlide()
          break
        default:
          break
      }
    }
  }

  /**
   * Create the header by adding markup to the current document's
   * DOM.
   */
  _createHeader () {
    this._navInnerHTML = ''
    this._generateNavigationSectionMarkup(this._currentDocument.body)

    const markup = '<header class="_vn-menu-bar">' +
          '<nav class="_vn-menu _vn-navigation-menu" role="navigation">' +
          '<h2 title="Open menu"><div class="_vn-menu-button">≡</div></h2>' +
          '<div class="_vn-menu-content">' +
          this._navInnerHTML +
          '<div class="_vn-viewbox"></div>' +
          '</div>' +
          '</nav>' +
          '<section class="_vn-menu  _vn-controls-menu" role="dialog" aria-label="Controls">' +
          '<h2 title="Open controls menu"><div class="_vn-menu-button">⚙</div></h2>' +
          '<form class="_vn-menu-content">' +
          '<fieldset>' +
          '<legend>Display</legend>' +
          '<label title="Normal (Ctrl+Shift+Alt+N)">' +
          '<input name="display" type="radio" value="normal" checked="true"/>' +
          'Normal</label>' +
          '<label title="Fullscreen (Ctrl+Shift+Alt+F)">' +
          '<input name="display" type="radio" value="fullscreen"/>' +
          'Fullscreen</label>' +
          '<label title="Slideshow (Ctrl+Shift+Alt+S)">' +
          '<input name="display" type="radio" value="slideshow"/>' +
          'Slideshow</label>' +
          '<label title="Presentation (Ctrl+Shift+Alt+P)">' +
          '<input name="display" type="radio" value="presentation"/>' +
          'Presentation</label>' +
          '<label title="Blank screen (Ctrl+Shift+Alt+B)">' +
          '<input name="display" type="radio" value="blank"/>' +
          'Blank</label>' +
          '</fieldset>' +
          '<fieldset>' +
          '<legend>Theme</legend>' +
          '<label title="Default theme">' +
          '<input name="theme" type="radio" value="default" checked="true"/>' +
          'Default</label>' +
          '<label title="Dark theme (Ctrl+Shift+Alt+D)">' +
          '<input name="theme" type="radio" value="dark"/>' +
          'Dark</label>' +
          '<label title="Light theme (Ctrl+Shift+Alt+L)">' +
          '<input name="theme" type="radio" value="light"/>' +
          'Light</label>' +
          '</fieldset>' +
          '</form>' +
          '</section>' +
          '</header>'

    this._currentDocument.body.insertAdjacentHTML('afterbegin', markup)

    this.menuBar = this._currentDocument.body.querySelector('header._vn-menu-bar')
    this._navMenu = this.menuBar.querySelector('nav._vn-menu')
    this._navMenuButton = this._navMenu.querySelector('._vn-menu-button')
    this._navMenuContent = this._navMenu.querySelector('div._vn-menu-content')
    this._viewbox = this._navMenuContent.querySelector('._vn-viewbox')

    this._controlsMenu = this.menuBar.querySelector('section._vn-menu')
    this._controlsMenuButton = this._controlsMenu.querySelector('._vn-menu-button')
    this._normalDisplayInput = this._controlsMenu.querySelector('input[value="normal"]')
    this._fullscreenDisplayInput = this._controlsMenu.querySelector('input[value="fullscreen"]')
    this._slideshowDisplayInput = this._controlsMenu.querySelector('input[value="slideshow"]')
    this._presentationDisplayInput = this._controlsMenu.querySelector('input[value="presentation"]')
    this._blankDisplayInput = this._controlsMenu.querySelector('input[value="blank"]')
    this._defaultThemeInput = this._controlsMenu.querySelector('input[value="default"]')
    this._darkThemeInput = this._controlsMenu.querySelector('input[value="dark"]')
    this._lightThemeInput = this._controlsMenu.querySelector('input[value="light"]')
  }

  /**
   * Generate the navigation menu markup by recursing through the
   * current documents sections and adding associated navigation menu
   * markup to the <code>_navInnerHTML</code> member variable.
   * @param {Element} sectionRoot - The element to scan for sections
   * and generate a navigation menu markup for.
   * @return {number} The number of sections added to the navigation
   * menu markup by this <code>sectionRoot</code>.
   */
  _generateNavigationSectionMarkup (sectionRoot) {
    const sections = sectionRoot.querySelectorAll(this._sectioningElementSelector)

    if (sections.length > 0) {
      this._navInnerHTML += '<' + this._listTagName + '>'

      for (let i = 0; i < sections.length; i++) {
        if (sections[i].id) {
          this._navInnerHTML += '<li><a href="#' + sections[i].id + '">' +
            sections[i].querySelector('h1, h2, h3, h4, h5, h6').innerHTML +
            '</a></li>'

          i += this._generateNavigationSectionMarkup(sections[i])
        }
      }

      this._navInnerHTML += '</' + this._listTagName + '>'
    }

    return sections.length
  }

  /**
   * Get the fraction of the provided element which is within the viewport.
   * @param {Element} element - The element to check.
   * @return {Array} - A two element array which gives the fraction of
   * the element which is above and below the viewport respectivley.
   */
  _getFractionsOfElementOffscreen (element) {
    const elementTop = element.offsetTop
    const elementBottom = elementTop + element.offsetHeight
    const viewportTop = window.scrollY
    const viewportBottom = viewportTop + window.innerHeight

    if (elementTop > viewportBottom) {
    /* Element is entirely below viewport. */
      return [0, 1]
    }

    if (elementBottom < viewportTop) {
    /* Element is entirely above viewport. */
      return [1, 0]
    }

    if (elementTop >= viewportTop && elementBottom <= viewportBottom) {
    /* Element entirely within viewport. */
      return [0, 0]
    }

    let fractionOfElementAboveViewport = 0
    let fractionOfElementBelowViewport = 0

    if (elementTop < viewportTop) {
    /* Top of element is above the viewport. */
      fractionOfElementAboveViewport = (viewportTop - elementTop) / (elementBottom - elementTop)
    }

    if (elementBottom > viewportBottom) {
    /* Bottom of element is below the viewport. */
      fractionOfElementBelowViewport = (elementBottom - viewportBottom) / (elementBottom - elementTop)
    }

    return [fractionOfElementAboveViewport, fractionOfElementBelowViewport]
  }
}

export { ViewportNavigation }

class SwipeDetector {
  constructor (tolerance) {
    this.tolerance = tolerance
    this._startTouch = null
    this._endTouch = null
  }

  addTouchEvent (touchEvent) {
    switch (touchEvent.type) {
      case 'touchstart':
        this._startTouch = touchEvent.changedTouches[0]
        break
      case 'touchend':
        this._endTouch = touchEvent.changedTouches[0]
        break
      case 'touchcancel':
        this._startTouch = null
        this._endTouch = null
        break
      default:
        // Do nothing
    }
  }

  isCompleted () {
    if (this._startTouch !== null &&
        this._endTouch !== null &&
        this._startTouch.identifier === this._endTouch.identifier) {
      return true
    } else {
      return false
    }
  }

  isSwipeLeft () {
    if (this.isCompleted() &&
        this._startTouch.screenX > this._endTouch.screenX + this.tolerance) {
      return true
    }

    return false
  }

  isSwipeRight () {
    if (this.isCompleted() &&
        this._startTouch.screenX < this._endTouch.screenX - this.tolerance) {
      return true
    }

    return false
  }
}
