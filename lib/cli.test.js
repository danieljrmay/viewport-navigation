const getClargs = require('./cli')

describe('Command line arguments', () => {
  test('Default output directory, single input file', () => {
    const testclargs = getClargs(['input.xhtml'])
    expect(testclargs.outputdir).toBe('vn-out')
    expect(testclargs._).toEqual(['input.xhtml'])
    expect(testclargs.debug).toBe(false)
  })

  test('Custom output directory, single input file', () => {
    const testclargs = getClargs(['--outputdir', 'myoutputdir', 'input.xhtml'])
    expect(testclargs.outputdir).toBe('myoutputdir')
    expect(testclargs._).toEqual(['input.xhtml'])
    expect(testclargs.debug).toBe(false)
  })

  test('Default output directory, two input files', () => {
    const testclargs = getClargs(['input1.xhtml', 'input2.xhtml'])
    expect(testclargs.outputdir).toBe('vn-out')
    expect(testclargs._).toEqual(['input1.xhtml', 'input2.xhtml'])
    expect(testclargs.debug).toBe(false)
  })

  test('Custom output directory, two input files', () => {
    const testclargs = getClargs(['--outputdir', 'myoutputdir', 'input1.xhtml', 'input2.xhtml'])
    expect(testclargs.outputdir).toBe('myoutputdir')
    expect(testclargs._).toEqual(['input1.xhtml', 'input2.xhtml'])
    expect(testclargs.debug).toBe(false)
  })

  test('Default output directory, single input file, debugging mode', () => {
    const testclargs = getClargs(['--debug', 'input.xhtml'])
    expect(testclargs.outputdir).toBe('vn-out')
    expect(testclargs._).toEqual(['input.xhtml'])
    expect(testclargs.debug).toBe(true)
  })

  test('Custom output directory, single input file, debugging mode', () => {
    const testclargs = getClargs(['-d', '--outputdir', 'myoutputdir', 'input.xhtml'])
    expect(testclargs.outputdir).toBe('myoutputdir')
    expect(testclargs._).toEqual(['input.xhtml'])
    expect(testclargs.debug).toBe(true)
  })

  test('Default output directory, two input files, debugging mode', () => {
    const testclargs = getClargs(['--debug', 'input1.xhtml', 'input2.xhtml'])
    expect(testclargs.outputdir).toBe('vn-out')
    expect(testclargs._).toEqual(['input1.xhtml', 'input2.xhtml'])
    expect(testclargs.debug).toBe(true)
  })

  test('Custom output directory, two input files, debugging mode', () => {
    const testclargs = getClargs(['-d', '--outputdir', 'myoutputdir', 'input1.xhtml', 'input2.xhtml'])
    expect(testclargs.outputdir).toBe('myoutputdir')
    expect(testclargs._).toEqual(['input1.xhtml', 'input2.xhtml'])
    expect(testclargs.debug).toBe(true)
  })

  test('Default output directory, no input files', () => {
    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {})
    const mockStdout = jest.spyOn(process.stdout, 'write').mockImplementation(() => {})
    getClargs([])
    expect(mockExit).toHaveBeenCalledWith(1)
    mockExit.mockRestore()
    mockStdout.mockRestore()
  })

  test('Custom output directory, no input files', () => {
    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {})
    const mockStdout = jest.spyOn(process.stdout, 'write').mockImplementation(() => {})
    getClargs(['--outputdir', 'myoutputdir'])
    expect(mockExit).toHaveBeenCalledWith(1)
    mockExit.mockRestore()
    mockStdout.mockRestore()
  })
})
