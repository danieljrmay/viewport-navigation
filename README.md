<!-- markdownlint-disable MD033 -->
# <img src="src/website/logo.png" width="100" alt="Logo"/> Viewport Navigation

[![pipeline
status](https://gitlab.com/danieljrmay/viewport-navigation/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/viewport-navigation/commits/master)
[![code style](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![npm](https://img.shields.io/npm/v/viewport-navigation)](https://www.npmjs.com/package/viewport-navigation)

*Viewport navigation* is a JavaScript library which enhances webpages
with a navigation menu and slideshow presentation functionality.

Find out more and see it in action at the [*viewport navigtion*
website](https://danieljrmay.gitlab.io/viewport-navigation/index.xhtml).

## Developing

Building and depoloying the various parts of this project is
controlled by scripts which are defined in the `package.json` file
which is run via `npm`.

```shell
# Lint the source files
npm run lint

# Create the built files in the dist directory
npm run build

# Create the documentation in the doc directory
npm run doc

# Run the test suite
npm run test

# Create the project's website in the public directory
npm run website

# Destroy the generated directories: dist, doc, public
npm run clean

# Destory all generated and downloaded files.
# Restore the sources to their pristine state.
npm run distclean

# Run a local HTTP server, for development purposes
npm run http-server
```
